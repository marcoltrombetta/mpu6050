# MPU6050 GUI

This application is designed to bring a user-interface for MPU6050 rotations values of the main 3-axis  in real-time.

Developed with Python GTK and compatible with:

- Serial port readings (Arduino)
- Pinephone 

## Installation requirements:

- Python +3
- Python GTK 3.0
- Python Matplotlib


## Installation:
